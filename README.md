# My NeoVim setup

## Installation
If starting NeoVim from terminal and it doesn't support 'true' colors:
install a terminal like [kitty](https://sw.kovidgoyal.net/kitty/quickstart/)

1. Install [NeoVim](https://neovim.io)
2. Install [a patched font for using icons](https://www.nerdfonts.com/font-downloads)
3. Install [packer](https://github.com/wbthomason/packer.nvim)
4. Check `lua/dylan/plugins-setup.lua` and save to install plugins

### Telescope live grep
Windows:
`winget install BurntSushi.ripgrep.MSVC`

Mac:
`brew install ripgrep`

## Startup theme
An optional theme for the startup plugin can be defined in `/env/startup-theme.lua`.

## TODO
- Fix DAP
