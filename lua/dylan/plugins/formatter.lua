local setup, formatter = pcall(require, "conform")

if not setup then
	return
end

formatter.setup({
	formatters_by_ft = {
		javascript = { "js-beautify" },
		typescript = { "js-beautify" },
		javascriptreact = { "js-beautify" },
		typescriptreact = { "js-beautify" },
		svelte = { "prettier" },
		css = { "js-beautify" },
		scss = { "js-beautify" },
		html = { "js-beautify" },
		json = { "prettier" },
		yaml = { "prettier" },
		markdown = { "prettier" },
		graphql = { "prettier" },
		lua = { "stylua" },
		sql = { "sql_formatter" },
    rust = { "rustfmt" },
    xml = { "xmlformatter" },
		-- python = { "isort", "black" },
	},
  formatters = {
    sql_formatter = {
      command = "sql-formatter",
      args = { "--language", "postgresql" },
    },
  },
	-- format_on_save = {
	-- 	lsp_fallback = true,
	-- 	timeout_ms = 5000,
	-- },
	notify_on_error = true,
})
