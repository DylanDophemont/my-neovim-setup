local setup, dap = pcall(require, "dap")
local os_info = vim.loop.os_uname()

if not setup then
  return
end

local dapui = require("dapui")

dap.listeners.after.event_initialized["dapui_config"] = function()
  dapui.open({})
end
dap.listeners.before.event_terminated["dapui_config"] = function()
  dapui.close({})
end
dap.listeners.before.event_exited["dapui_config"] = function()
  dapui.close({})
end

-- Set the dap icons
local sign = vim.fn.sign_define
sign('DapBreakpoint', { text = '●', texthl = 'DapBreakpoint', linehl = '', numhl = ''})
sign('DapStopped', {text='●', texthl='DapStopped', linehl='', numhl=''})
sign('DapBreakpointRejected', {text='●', texthl='DapBreakpointRejected', linehl='', numhl=''})
sign('DapBreakpointCondition', { text = '●', texthl = 'DapBreakpointCondition', linehl = '', numhl = ''})
sign('DapLogPoint', { text = '◆', texthl = 'DapLogPoint', linehl = '', numhl = ''})

-- dap.set_log_level("DEBUG")

-- Adapters
-- https://github.com/mfussenegger/nvim-dap/wiki/Debug-Adapter-installation#javascript

dap.adapters["pwa-node"] = {
  type = "server",
  host = "localhost",
  port = "${port}",
  executable = {
    command = "node",
    args = {
      os_info.sysname == "Windows_NT" and
        'D:/js-debug/src/dapDebugServer.js' or
        os.getenv('HOME') .. '/js-debug/src/dapDebugServer.js',
      "${port}"
    },
  }
}

dap.adapters.firefox = {
  type = 'executable',
  command = 'node',
  args = {
    os_info.sysname == "Windows_NT" and
      'D:\\vscode-firefox-debug\\dist\\adapter.bundle.js' or
      os.getenv('HOME') .. '/vscode-firefox-debug/dist/adapter.bundle.js'
  },
}

-- Pre-launch task
dap.listeners.on_config["pwa-node"] = function(config)
  if (config.name == "Launch nest app") then
    vim.fn.system("nest build")
    print("Building nest app")
  end
  return config
end

-- Configurations typescript and javascript
for _, language in ipairs({ "typescript", "javascript" }) do
  dap.configurations[language] = {
    {
      name = "Launch nest app",
      type = "pwa-node",
      request = "launch",
      program = "${workspaceFolder}/dist/main.js",
      cwd = "${workspaceFolder}",
      sourceMaps = true,
      console = "integratedTerminal",
      protocol = "inspector",
    },
    {
      name = "Attach nest app",
      type = "pwa-node",
      request = "attach",
      processId = require'dap.utils'.pick_process,
      cwd = "${workspaceFolder}",
      protocol = "inspector",
      sourceMaps = true,
      console = "integratedTerminal",
      internalConsoleOptions = "neverOpen",
      skipFiles = { "<node_internals>/**" },
    },
    {  
      name = 'Launch Angular',
      type = 'firefox',
      request = 'launch',
      reAttach = true,
      url = 'http://localhost:4200/',
      webRoot = '${workspaceFolder}',
      firefoxExecutable = os_info.sysname == "Windows_NT" and 'C:\\Program Files\\Mozilla Firefox\\firefox.exe' or '/Applications/Firefox.app/Contents/MacOS/firefox -start-debugger-server'
    },
    {  
      name = 'Attach Angular',
      type = 'firefox',
      request = 'attach',
      reAttach = true,
      url = 'http://localhost:4200/',
      webRoot = '${workspaceFolder}',
      firefoxExecutable = os_info.sysname == "Windows_NT" and 'C:\\Program Files\\Mozilla Firefox\\firefox.exe' or '/Applications/Firefox.app/Contents/MacOS/firefox -start-debugger-server'
    },
    {  
      name = 'Launch Firefox',
      type = 'firefox',
      request = 'launch',
      reAttach = true,
      url = 'http://localhost:5173/',
      webRoot = '${workspaceFolder}/src',
      firefoxExecutable = os_info.sysname == "Windows_NT" and 'C:\\Program Files\\Mozilla Firefox\\firefox.exe' or '/Applications/Firefox.app/Contents/MacOS/firefox'
    },
    {  
      name = 'Attach Firefox',
      type = 'firefox',
      request = 'attach',
      reAttach = true,
      url = 'http://localhost:5173/',
      webRoot = '${workspaceFolder}/src',
      firefoxExecutable = os_info.sysname == "Windows_NT" and 'C:\\Program Files\\Mozilla Firefox\\firefox.exe' or '/Applications/Firefox.app/Contents/MacOS/firefox'
    }
  }
end

