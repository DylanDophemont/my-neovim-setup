local status, dadbod = pcall(require, "dadbod")
if not status then
  return
end

local status, dadbodUI = pcall(require, "dadbod-ui")
if not status then
  return
end

local status, dadbodCompletion = pcall(require, "dadbod-completion")
if not status then
  return
end

dadbod.setup()
dadbodUI.setup()
dadbodCompletion.setup()
