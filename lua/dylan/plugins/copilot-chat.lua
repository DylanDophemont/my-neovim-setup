local setup, cpc = pcall(require, "CopilotChat")

if not setup then
  return
end

cpc.setup({
  mappings = {
    show_diff = {
      normal = "<C-i>",
    }
  }
})
