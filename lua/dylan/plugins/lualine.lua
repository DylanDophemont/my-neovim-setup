local status, lualine = pcall(require, "lualine")
if not status then
  return
end

lualine.setup {
  options = {
    theme = "gruvbox-material",
    icons_enabled = false,
  },
  sections = {
    lualine_c = {
      {
        'filename',
        path = 1
      }
    }
  },
  inactive_sections = {
    lualine_c = {
      {
        'filename',
        path = 1
      }
    },
    lualine_x = {'location'},
  }
}
