local setup, startup = pcall(require, "startup")

local themefound, envTheme = pcall(require, "env.startup-theme")

if not setup then
  return
end

if themefound then
  startup.setup( envTheme )
else
  startup.setup({
    theme = 'startify'
  })
end

