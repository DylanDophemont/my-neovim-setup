local status, todo = pcall(require, "todo-comments")
if not status then
  return
end

local defaultPattern = [[.*<(KEYWORDS)\s*]]

todo.setup({
  keywords = {
    TEMP = { icon = " ", color = "temp", alt = { "TEMPORARY", "TEMP" } },
  },
  highlight = {
    pattern = defaultPattern
  },
  colors = {
    temp = { "Identifier", "#FF9933" }
  },
  search = {
    pattern = defaultPattern
  }
})
