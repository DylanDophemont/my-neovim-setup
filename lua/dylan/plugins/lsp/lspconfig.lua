local lspconfig_status, lspconfig = pcall(require, 'lspconfig')
if not lspconfig_status then
  return
end

local cmp_nvim_lsp_status, cmp_nvim_lsp = pcall(require, 'cmp_nvim_lsp')
if not cmp_nvim_lsp_status then
  return
end

local keymap = vim.keymap

local on_attach = function(client, bufnr)
  vim.lsp.inlay_hint.enable(true, { bufnr = bufnr })
end

local capabilities = require('cmp_nvim_lsp').default_capabilities()

-- configure html server with plugin
lspconfig['html'].setup({
  capabilities = capabilities,
  on_attach = on_attach
})

-- configure typescript server with plugin
lspconfig['ts_ls'].setup({
  server = {
    capabilities = capabilities,
    on_attach = on_attach,
  },
})

-- configure css server
lspconfig['cssls'].setup({
  capabilities = capabilities,
  on_attach = on_attach,
})


-- configure rust server
lspconfig['rust_analyzer'].setup({
  capabilities = capabilities,
  on_attach = on_attach,
})

lspconfig['lemminx'].setup({
  capabilities = capabilities,
  on_attach = on_attach,
})

lspconfig.rust_analyzer.setup({})

