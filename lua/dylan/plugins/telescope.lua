local telescope_setup, telescope = pcall(require, 'telescope')

if not telescope_setup then
  return
end

local actions_setup, actions = pcall(require, 'telescope.actions')
if not actions_setup then
  return
end

telescope.setup({
  defaults = {
    file_ignore_patterns = { 'node_modules', '^.git[/\\]', 'vendor', 'dist' },
    mappings = {
      i = {
        ['<C-k>'] = actions.move_selection_previous,
        ['<C-j>'] = actions.move_selection_next,
        ['<C-q>'] = actions.send_selected_to_qflist + actions.open_qflist,
        ["<C-q>"] = function(prompt_bufnr)
          actions.send_to_qflist(prompt_bufnr)
          actions.open_qflist()
        end,
      },
      n = {
        ["<C-q>"] = function(prompt_bufnr)
          actions.send_to_qflist(prompt_bufnr)
          actions.open_qflist()
        end,
      },
    }
  }
})

-- telescope.load_extension('fzf')
