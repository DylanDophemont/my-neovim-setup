vim.g.mapleader = " "

local os_info = vim.loop.os_uname()
local keymap = vim.keymap

-- general keymaps
-- keymap.set('n', '<leader>sv', '<C-w>v')
-- keymap.set('n', '<leader>sh', '<C-w>s')
-- keymap.set('n', '<leader>sx', ':close<CR>')
keymap.set("n", "<leader>/", ':let @/ = ""<CR>')

-- tree toggle
keymap.set("n", "<leader>e", ":NvimTreeToggle<CR>")

-- Telescope
keymap.set("n", "<leader>ff", "<cmd>Telescope find_files hidden=true no_ignore=true <cr>")
keymap.set("n", "<leader>fg", "<cmd>Telescope live_grep<cr>")
keymap.set("n", "<leader>fw", "<cmd>Telescope grep_string<cr>")
keymap.set("n", "<leader>fb", "<cmd>Telescope buffers<cr>")
keymap.set("n", "<leader>fh", "<cmd>Telescope help_tags<cr>")
keymap.set("n", "<leader>fc", "<cmd>Telescope git_bcommits<cr>")
keymap.set("n", "<leader>fr", "<cmd>Telescope git_branches<cr>")
keymap.set("n", "<leader>fo", "<cmd>Telescope colorscheme enable_preview=true<cr>")

-- terminal in split window
keymap.set("n", "<C-t>", ":vsplit +term<cr>")
keymap.set("t", "<C-t>", [[<C-\><C-n>]])

-- lsp
local opts = { noremap = true, silent = true, buffer = bufnr }
keymap.set("n", "gf", "<cmd>Lspsaga lsp_finder<CR>", opts) -- show definition, references
keymap.set("n", "gD", "<Cmd>lua vim.lsp.buf.declaration()<CR>", opts) -- got to declaration
keymap.set("n", "<leader>pd", "<cmd>Lspsaga peek_definition<CR>", opts) -- see definition and make edits in window
keymap.set("n", "gi", ":lua vim.lsp.buf.implementation()<CR>", opts) -- go to implementation
keymap.set("n", "gv", ":vsplit<CR>:lua vim.lsp.buf.implementation()<CR>", opts) -- go to implementation in vertical split
keymap.set("n", "<leader>lr", "<cmd>lua vim.lsp.buf.references()<CR>", opts) -- list references
keymap.set("n", "<leader>ca", "<cmd>Lspsaga code_action<CR>", opts) -- see available code actions
keymap.set("n", "<leader>rn", "<cmd>Lspsaga rename<CR>", opts) -- smart rename
keymap.set("n", "<leader>D", "<cmd>Lspsaga show_line_diagnostics<CR>", opts) -- show  diagnostics for line
keymap.set("n", "<leader>d", "<cmd>Lspsaga show_cursor_diagnostics<CR>", opts) -- show diagnostics for cursor
keymap.set("n", "[d", "<cmd>Lspsaga diagnostic_jump_prev<CR>", opts) -- jump to previous diagnostic in buffer
keymap.set("n", "]d", "<cmd>Lspsaga diagnostic_jump_next<CR>", opts) -- jump to next diagnostic in buffer
keymap.set("n", "K", "<cmd>Lspsaga hover_doc<CR>", opts) -- show documentation for what is under cursor
keymap.set("n", "<leader>o", "<cmd>Lspsaga outline<CR>", opts) -- see outline on right hand side

keymap.set("n", "<leader>ru", function()
  vim.lsp.buf.code_action({
    apply = true,
    context = { only = { "source.removeUnusedImports" } }, -- VTSLS-specific action
  })
end, { desc = "Remove unused imports", noremap = true, silent = true })

-- dap debugger
keymap.set("n", "<leader>dc", ":DapContinue<cr>")
keymap.set("n", "<Leader>bp", function() require("dap").toggle_breakpoint(vim.fn.input("Breakpoint at condition:"), nil) end)
keymap.set("n", "<leader>si", ":DapStepInto<cr>")
keymap.set("n", "<leader>so", ":DapStepOver<cr>")
keymap.set("n", "<leader>dt", ":DapTerminate<cr>")
-- keymap.set('n', '<leader>dso', ':DapStepOut<cr>')
keymap.set("n", "<leader>dui", require("dapui").toggle)

-- change light/dark
keymap.set("n", "<leader>bgl", ":set background=light<cr>")
keymap.set("n", "<leader>bgd", ":set background=dark<cr>")

-- HTTP rest testing
vim.keymap.set("n", "<leader>rc", "<Plug>RestNvim")

-- Copilot chat
vim.keymap.set("n", "<leader>cc", ":CopilotChat<cr>")
vim.keymap.set("v", "<leader>cf", ":CopilotChatFix<cr>")
vim.keymap.set("v", "<leader>ce", ":CopilotChatExplain<cr>")
vim.keymap.set("v", "<leader>co", ":CopilotChatOptimize<cr>")

-- Encapsulate word with char
vim.api.nvim_set_keymap("n", "<leader>a", ":lua EncapsulateWord()<CR>", { noremap = true, silent = true })

function EncapsulateWord()
	local char = vim.fn.input("character: ")
	vim.api.nvim_feedkeys("e" .. "a" .. char .. "" .. "bi" .. char .. "", "n", false)
end

-- Replacement for only
vim.api.nvim_set_keymap("n", "<C-W>o", ":tab split<CR>", { noremap = true, silent = true })

-- Move selection up/down
vim.api.nvim_set_keymap("v", "<C-k>", ":m '<-2<CR>gv=gv", { noremap = true, silent = true })
vim.api.nvim_set_keymap("v", "<C-j>", ":m '>+1<CR>gv=gv", { noremap = true, silent = true })

-- zoom
local defaultFontSize = (os_info.sysname == "Windows_NT") and 10 or 12
local fontSize = defaultFontSize -- Start with a default size

function _G.ResetFontSize()
  fontSize = defaultFontSize
  vim.api.nvim_command('GuiFont! 0xProto Nerd Font Mono:h' .. fontSize)
end

function _G.IncreaseFontSize()
  fontSize = fontSize + 1
  vim.api.nvim_command('GuiFont! 0xProto Nerd Font Mono:h' .. fontSize)
end

function _G.DecreaseFontSize()
  if fontSize > 1 then
    fontSize = fontSize - 1
    vim.api.nvim_command('GuiFont! 0xProto Nerd Font Mono:h' .. fontSize)
  end
end

if not vim.g.neovide then
  vim.api.nvim_set_keymap("n", "<C-0>", ":lua ResetFontSize()<CR>", { noremap = true, silent = true })
  vim.api.nvim_set_keymap("n", "<C-->", ":lua DecreaseFontSize()<CR>", { noremap = true, silent = true })
  vim.api.nvim_set_keymap("n", "<C-=>", ":lua IncreaseFontSize()<CR>", { noremap = true, silent = true })
end

-- formatter
vim.api.nvim_set_keymap(
	"n",
	"<leader>af",
	":lua require('conform').format({timeout_ms = 5000, lsp_fallback = true})<CR>",
	{ noremap = true, silent = true }
)

-- gitsigns
vim.api.nvim_set_keymap("n", "]c", ":Gitsigns next_hunk<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "[c", ":Gitsigns prev_hunk<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>gr", ":Gitsigns reset_hunk<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>gp", ":Gitsigns preview_hunk<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>gb", ":Gitsigns blame_line<CR>", { noremap = true, silent = true })
vim.api.nvim_set_keymap("n", "<leader>gd", ":Gitsigns diffthis<CR>", { noremap = true, silent = true })

-- todo
vim.keymap.set("n", "]t", function()
	require("todo-comments").jump_next()
end, { desc = "Next todo comment" })

vim.keymap.set("n", "[t", function()
	require("todo-comments").jump_prev()
end, { desc = "Previous todo comment" })

-- orientate all windows vertically
vim.api.nvim_set_keymap('n', '<leader>wh', ':windo wincmd K<CR>', { noremap = true, silent = true })

-- orientate all windows horizontally
vim.api.nvim_set_keymap('n', '<leader>wv', ':windo wincmd H<CR>', { noremap = true, silent = true })

-- rust test
vim.keymap.set("n", "<leader>rt", ":RustTest<CR>", { desc = "Run rust test" })

