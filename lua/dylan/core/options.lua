local opt = vim.opt -- for conciseness
local os_info = vim.loop.os_uname()

-- line numbers
opt.relativenumber = false
opt.number = true

-- formatting
opt.formatoptions:remove("o")

-- tabs & indentation
opt.tabstop = 2
opt.shiftwidth = 2
opt.expandtab = true
opt.autoindent = true

-- line wrapping
opt.wrap = false

-- search settings
opt.ignorecase = true
opt.smartcase = true

-- cursor line
opt.cursorline = true

-- appearance
opt.termguicolors = true
opt.background = "dark"
opt.signcolumn = "yes"

-- backspace
opt.backspace = "indent,eol,start"

-- clipboard
opt.clipboard:append("unnamedplus")

-- split windows
opt.splitright = true
opt.splitbelow = true

-- folding
vim.o.foldmethod = "syntax"
vim.o.foldlevelstart = 99

--opt.iskeyword:append("-")
-- hello-hello

if os_info.sysname == "Windows_NT" then
  opt.shell = 'powershell.exe'
  opt.shellcmdflag = '-command'
  opt.shellquote='\"'
end

-- Enable cursor blinking
vim.o.guicursor = 'a:blinkwait500-blinkoff300-blinkon300'

-- Show folder name in window title
opt.title = true
vim.cmd([[
  autocmd DirChanged * let &titlestring = fnamemodify(getcwd(), ":t")
]])

