-- initial colorscheme when staring neovim
-- require("dylan.core.colorschemes.noirbuddy")
-- require("dylan.core.colorschemes.modus")
-- require("dylan.core.colorschemes.everforest")
require("dylan.core.colorschemes.gruvbox")
-- require("dylan.core.colorschemes.kaodam")
-- require("dylan.core.colorschemes.kanagawa")
-- require("dylan.core.colorschemes.onenord")
-- require("dylan.core.colorschemes.catppuccin")
