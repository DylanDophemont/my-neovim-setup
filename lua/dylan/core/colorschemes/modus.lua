local colorSchemeName = "modus"
local status, colorScheme = pcall(require, "modus-themes")

if not status then
  print("Colorscheme not found!")
  return
end

colorScheme.setup {
  dim_inactive = true,
}

vim.cmd("colorscheme "..colorSchemeName)

