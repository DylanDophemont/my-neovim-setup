local colorSchemeName = "kanagawa"

vim.cmd("colorscheme "..colorSchemeName)

local status, kanagawa = pcall(require, colorSchemeName)

if not status then
  print("Colorscheme not found!")
  return
end

kanagawa.setup()
