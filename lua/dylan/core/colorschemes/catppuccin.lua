require("catppuccin").setup({
	-- default_integrations = true,
	dim_inactive = {
		enabled = true,
		shade = "dark",
		percentage = 0.15, -- percentage of the shade to apply to the inactive window
	},
	integrations = {
		dap = true,
		dap_ui = true,
	},
})

vim.cmd.colorscheme("catppuccin")
