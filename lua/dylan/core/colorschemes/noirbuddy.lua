local darkColorSchemeName = "noirbuddy"
local status, darkColorScheme = pcall(require, darkColorSchemeName)

if not status then
  print("Colorscheme not found!")
  return
end

vim.cmd("colorscheme "..darkColorSchemeName)

darkColorScheme.setup {
  preset = 'minimal',
  colors = {
    primary = '#ff0000',
  }
}

