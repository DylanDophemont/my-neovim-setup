local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins-setup.lua source <afile> | PackerSync
  augroup end
]])

local status, packer = pcall(require, "packer")
if not status then
  return
end

return packer.startup(function(use)
  use 'wbthomason/packer.nvim'

  -- generic plugins
  use 'nvim-lua/plenary.nvim'
  use 'numToStr/Comment.nvim'
  use 'nvim-lualine/lualine.nvim' -- use 'nvim-tree/nvim-web-devicons'
  use 'nvim-tree/nvim-tree.lua'
  use 'nvim-treesitter/nvim-treesitter'

  -- themes
  -- use 'folke/tokyonight.nvim'
  use 'miikanissi/modus-themes.nvim'
  use 'neanias/everforest-nvim'
  use 'sainnhe/gruvbox-material'
  use { "jesseleite/nvim-noirbuddy", requires = { "tjdevries/colorbuddy.nvim" } }
  use 'saulhoward/kaodam'
  use "rebelot/kanagawa.nvim"
  use "slugbyte/lackluster.nvim"
  use 'rmehri01/onenord.nvim'
  use { "catppuccin/nvim", as = "catppuccin" }

  -- todo tags
  use { "folke/todo-comments.nvim", requires = { 'nvim-lua/plenary.nvim' } }

  -- telescope
  if vim.fn.has('win32') or vim.fn.has('win64') then
    use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build' }
  else
    use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make'}
  end
  use {'nvim-telescope/telescope.nvim', branch = '0.1.x', requires = { {'nvim-lua/plenary.nvim'} }}

  -- auto completion
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-buffer'
  use 'hrsh7th/cmp-path'
  use 'L3MON4D3/LuaSnip'
  use 'rafamadriz/friendly-snippets'

  -- managing & installing lsp servers
  use 'williamboman/mason.nvim'
  use 'williamboman/mason-lspconfig.nvim'

  -- configuring lsp servers
  use 'neovim/nvim-lspconfig'
  use 'hrsh7th/cmp-nvim-lsp'
  use {'glepnir/lspsaga.nvim', branch = 'main'}
  use 'onsails/lspkind.nvim'

  -- git signs
  use 'lewis6991/gitsigns.nvim'

  -- debugger
  use 'mfussenegger/nvim-dap'
  use { "mxsdev/nvim-dap-vscode-js", requires = {"mfussenegger/nvim-dap"} }
  use { "rcarriga/nvim-dap-ui", requires = {"mfussenegger/nvim-dap", "nvim-neotest/nvim-nio"} }

  -- startup splash screen
  use { "startup-nvim/startup.nvim", requires = {"nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim"} }

  -- highlight color values
  use 'norcalli/nvim-colorizer.lua'

  -- indent guides
  use "lukas-reineke/indent-blankline.nvim"

  -- dadbod dbms
  use 'tpope/vim-dadbod'
  use 'kristijanhusak/vim-dadbod-ui'
  use 'kristijanhusak/vim-dadbod-completion'

  -- The terminator movie all over again
  use 'github/copilot.vim'
  use 'CopilotC-Nvim/CopilotChat.nvim'

  -- handy mappings for buffers and such
  use 'tpope/vim-unimpaired'

  -- formatting
  use 'stevearc/conform.nvim'
  use 'beautify-web/js-beautify'

  -- rust
  use 'rust-lang/rust.vim'

  -- Git fugitive
  use 'tpope/vim-fugitive'

  -- Guess indentation
  use {
    'nmac427/guess-indent.nvim',
    config = function() require('guess-indent').setup {} end,
  }

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)
